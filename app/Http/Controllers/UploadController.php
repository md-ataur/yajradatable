<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function index()
    {
        $files = File::all();         
        return view('upload.upload',compact('files'));
    }

    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            //return $request->file('image');
            //return $request->image->path();
            //return $request->image->store('public');
            //return $request->image->storeAs('public','img.jpg');
            //return Storage::putFile('public',$request->file('image'));
            
            $filename = $request->file('image')->getClientOriginalName();            
            $request->image->storeAs('public/upload', $filename);
            $filesize = Storage::size('public/upload/'.$filename);
            $file = new File;
            $file->name = $filename;
            $file->size = $filesize;
            $file->save();
            return \redirect('/upload');

        }else{
            return "No file selected";
        }
    }

}
