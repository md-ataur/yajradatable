<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\DataTables\UsersDataTable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {       
        return view('home');
    }
    
    public function getUsers(){
        return Datatables::of(User::query())        
            /* class add inside the row */
            ->setRowClass('{{ $id % 2 == 0 ? "alert-success" : "alert-warning" }}')
            /* column edit  */
            ->editColumn('created_at', function(User $user){
                return $user->created_at->diffForHumans();
            })
            ->make(true);
    }

    
    /*public function dataTable(UsersDataTable $dataTable)
    {
         return $dataTable->render('index');
    }*/
  

}
