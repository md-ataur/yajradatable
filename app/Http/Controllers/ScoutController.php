<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ScoutController extends Controller
{
    public function search($searchkey)
    {
        $users = User::search($searchkey)->get();
        //return $users;
        return view('search',compact('users'));
    }
}
