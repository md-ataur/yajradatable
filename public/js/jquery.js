(function ($) {
	$(document).ready(function ($) {
		$('#userForm').on('submit', function (event) {
			event.preventDefault();
			$.ajax({
				url: 'ajax-crud',
				method: "POST",
				data: new FormData(this), /* From Data Object it will send form data to server */
				contentType: false, /* I has been used when sending data to the server */
				cache: false, /* It will unable to cache requested pages */
				processData: false,
				dataType: "json",
				success: function (data) {
					console.log(data);
					var html = '';

					if (data.errors) {
						html = '<div>';
						for (var count = 0; count < data.errors.length; count++) {
							html += '<p class="alert alert-danger">' + data.errors[count] + '</p>';
						}
						html += '</div>';
					}

					if (data.success) {
						html = '<div class="alert alert-success">' + data.success + '</div>';
						$('#userForm')[0].reset();
						$('#store_image').html('');
						//$('#user_table').DataTable().ajax.reload();
					}
					$('#form_result').html(html);

				}
			});
		});
	});

})(jQuery);
