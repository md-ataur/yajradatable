<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ajax Crud</title>
    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">   
</head>
<body>
    <div class="container">
        <br/>
        <h3 align="center">Laravel Ajax Crud System with Databale</h3>    
        <div align="right">
            <button type="button" name="create_record" id="create_record" data-toggle="modal" data-target="#formModal" class="btn btn-sm btn-success">Create Record</button>
        </div>
        <br/>
        <div class="table-bar">
            <table class="table" id="user_table">
                <thead>
                    <tr>
                        <th width="5%">Id</th>
                        <th width="10%">Image</th>
                        <th width="30%">First Name</th>
                        <th width="30%">Last Name</th>
                        <th width="35%">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <form action="" method="post" id="userForm" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add New Record</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>                
                    <div class="modal-body">                        
                        <span id="form_result"></span>                  
                        @csrf
                        <div class="form-group">
                            <label for="firstname" class="control-label">First Name:</label>
                            <div>
                                <input type="text" name="first_name" id="first_name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Last Name:</label>
                            <div>
                                <input type="text" name="last_name" id="last_name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Select Profile Image:</label>
                            <div>
                                <input type="file" name="image" id="image">
                                <span id="store_image"></span>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <input type="hidden" name="id" id="id">                            
                        </div>                    
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <input type="hidden" name="_method" id="update_method" />
                        <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
                    </div>                                    

                </div>
            </div>
        </div>
        <div id="confirmModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">                    
                    <div class="modal-body">
                        <span id="delete_result"></span>
                        <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
                    </div>
                    <div class="modal-footer">                        
                        <input type="submit" name="delete_action" id="delete_button" class="btn btn-danger" value="Ok" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="/js/jquery-3.4.1.min.js"></script>
    <script src="/js/jquery.dataTables.min.js" defer></script>    
    <script src="/js/bootstrap.min.js"></script> 
    {{-- <script src="/js/jquery.js"></script>--}}
    <script>   

    $('#create_record').click(function(){
        $('.modal-title').text("Add New Record");        
        $('#action_button').val("Add");        
        $('#update_method').val("");
        $('#first_name').val("");
        $('#last_name').val("");
        $('#store_image').html('');
        $('#form_result').html('');
    });


    /* Data show inside the datatable */
    $(document).ready(function(){
        $('#user_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{!! route('ajax-crud.index') !!}",
            columns: [          
                { data: 'id', name: 'id' },      
                {
                    data: 'image',
                    name: 'image',
                    render: function(data, type, full, meta){
                    return "<img src={{ URL::to('/') }}/images/" + data + " width='70' class='img-thumbnail' />" ; }, orderable: false 
                },          
                { data: 'first_name', name: 'first_name' },
                { data: 'last_name', name: 'last_name' },                 
                { data: 'action', name: 'action', orderable: false },
                
            ]
        });

    });

    
    /* Data insert inside the database */
    $('#userForm').on('submit',function(event){
        event.preventDefault();        
        if($('#action_button').val() == 'Add'){
            $.ajax({
                url:"{{ route('ajax-crud.store') }}",
                method:"POST",
                data: new FormData(this), /* From Data Object it will send form data to server */
                contentType: false, /* I has been used when sending data to the server */
                cache:false, /* It will unable to cache requested pages */
                processData: false, 
                dataType:"json",
                success:function(data){
                    console.log(data);
                    var html = '';

                    if(data.errors){
                        html = '<div>';
                        for(var count = 0; count < data.errors.length; count++){ 
                            html +='<p class="alert alert-danger">' + data.errors[count] + '</p>' ; 
                        } 
                        html +='</div>' ; 
                    }                
                    if(data.success){
                        html = '<div class="alert alert-success">' + data.success + '</div>';
                        $('#userForm')[0].reset();
                        $('#store_image').html('');
                        $('#user_table').DataTable().ajax.reload();
                        
                    }
                    $('#form_result').html(html);

                }
            });
        }

        /* Data Update to the database */
        if($('#action').val() == "Edit"){  
            var id = $('#hidden_id').val();            
            $.ajax({
                url:"/ajax-crud/"+id,
                method:"POST",
                data:new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType:"json",
                success:function(data){
                    console.log(data);
                    var html = '';
                    if(data.errors){
                        html = '<div>';
                        for(var count = 0; count < data.errors.length; count++){ 
                            html +='<p class="alert alert-danger">' + data.errors[count] + '</p>';
                        } 
                        html +='</div>';
                    } 
                    if(data.success){ 
                        html='<div class="alert alert-success">' + data.success + '</div>';
                        $('#userForm')[0].reset();
                        $('#store_image').html(''); 
                        $('#user_table').DataTable().ajax.reload(); 
                    }
                    $('#form_result').html(html);
                } 
            }); 
        }

        /* Data Delete from the database */
        if($('#action').val() == "Delete"){  
            var id = $('#hidden_id').val();            
            $.ajax({
                url:"/ajax-crud/"+id,
                method:"POST",
                data:new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType:"json",                
                success:function(data){
                    console.log(data);
                    var html = '';
                    if(data.errors){
                        html = '<div>';
                        for(var count = 0; count < data.errors.length; count++){ 
                            html +='<p class="alert alert-danger">' + data.errors[count] + '</p>';
                        } 
                        html +='</div>';
                    } 
                    if(data.success){ 
                        html='<div class="alert alert-danger">' + data.success + '</div>';                       
                        setTimeout(function(){
                            $('#confirmModal').modal('hide');
                            $('#user_table').DataTable().ajax.reload();
                        }, 1000);
                    }
                    $('#delete_result').html(html);
                } 
            }); 
        }

    });


    /* Data delete */
    $(document).on('click', '.delete', function(){
        $('#confirmModal').modal('show');
        var id = $(this).attr('id');
        $('#action').val("Delete");
        $('#hidden_id').val(id);
        $('#update_method').val("DELETE");
        $('#delete_result').html('');
        //console.log(id);        

    });

    /* Data fetch from the database */
    $(document).on('click', '.edit', function(){
        var id = $(this).attr('id');        
        $('#form_result').html('');
        $.ajax({
            url:"/ajax-crud/"+id+"/edit",
            dataType:"json",
            success:function(html){
                //console.log(html);
                $('#first_name').val(html.data.first_name);
                $('#last_name').val(html.data.last_name);
                $('#store_image').html("<img src={{ URL::to('/') }}/images/" + html.data.image + " width='70' class='img-thumbnail' />");
                $('#store_image').append("<input type='hidden' name='hidden_image' value='"+html.data.image+"' />");
                $('#hidden_id').val(html.data.id);
                $('.modal-title').text("Edit Record");
                $('#action_button').val("Update");
                $('#update_method').val("PUT");
                $('#action').val("Edit");
                $('#formModal').modal('show');
            }
        })
    });
   
    
    </script>
</body>
</html>