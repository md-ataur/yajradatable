<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Upload</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">   
</head>
<body>
    <div class="container">        
        <br>
        <div class="col-md-6 offset-3">
            <center><h3>Upload File</h3></center>
            <hr>
            <form action="/store" enctype="multipart/form-data" method="post">
                @csrf
                <input type="file" name="image" class="form-controll">                
                <input type="submit" value="Upload" name="submit">
            </form>
            <hr>
            <div>
                @foreach ($files as $file)
                    <p><img width="100" height="100" src="{{ URL::asset('storage/upload/' . $file->name) }}"></p>
                @endforeach
            </div>
        </div>
    </div>
</body>
</html>